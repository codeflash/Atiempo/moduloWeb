<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFotosempresaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('fotosempresa', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->text('fotos', 65535)->nullable();
			$table->integer('idEmpresas')->index('fk_fotosEmpresa_empresas1_idx');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('fotosempresa');
	}

}
