<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDetallerutaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('detalleruta', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('idEmpresa')->index('fk_Empresas_has_Rutas_Empresas_idx');
			$table->integer('idRuta')->index('fk_Empresas_has_Rutas_Rutas1_idx');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('detalleruta');
	}

}
