<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name')->nullable();
			$table->string('apellidos', 100)->nullable();
			$table->string('alias', 100)->nullable();
			$table->string('password')->nullable();
			$table->string('email')->nullable()->unique('correo_UNIQUE');
			$table->enum('tipo', array('E','R'))->nullable()->default('R');
			$table->text('foto', 65535)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
