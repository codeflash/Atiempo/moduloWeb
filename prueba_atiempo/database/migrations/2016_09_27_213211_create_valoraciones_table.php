<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateValoracionesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('valoraciones', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('puntuacion')->nullable();
			$table->string('descripcion', 200)->nullable();
			$table->dateTime('fecha')->nullable();
			$table->integer('idUsuario')->index('fk_valoraciones_usuarios1_idx');
			$table->integer('empresas_id')->index('fk_valoraciones_empresas1_idx');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('valoraciones');
	}

}
