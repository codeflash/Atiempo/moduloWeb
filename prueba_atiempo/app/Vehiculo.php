<?php

namespace ATiempo;

use Illuminate\Database\Eloquent\Model;

class Vehiculo extends Model
{
	protected $table = "vehiculos";
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'placa', 'idEmpresa', 'latitud', 'longitud',
    ];
}
