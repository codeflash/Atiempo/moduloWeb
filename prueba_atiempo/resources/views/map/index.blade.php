@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div style="height:500px; width:500px" id="map">
            
            </div>
            <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
            <input type="text" id="lat">
            <input type="text" id="lng">
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script>
 
 var map;
var marker;          //variable del marcador
var lng = 0;
var lat = 0;



$(document).on("ready", function(){
 
  //cargarBuses();
   $.ajaxSetup({"cache" : false});
  //setInterval(function(){ alert("Hello"); }, 3000);
  setInterval(cargarBuses ,500);

  //initMap();
  
  //setInterval("createMarker()" ,500);

});

   //coordenadas obtenidas con la geolocalización
function cargarBuses(){
      var token = $("#token").val();
      $.ajax({
        url : "http://localhost:8000/map",
        headers : {'X-CSRF-TOKEN' : token},
        type : "PUT",
        dataType : "json",
      }).done(function(data){
          lng = parseFloat(data.longitud) ;
          lat = parseFloat(data.latitud) ;
          if (typeof(map) === "undefined") {
                initMap();
          }
          createMarker();
      });
};
      
//Funcion principal
function initMap() 
{
        
        //console.log(lng);
         map = new google.maps.Map(document.getElementById('map'),
          {
            zoom: 16,
            center:new google.maps.LatLng(lat,lng),
     
          });
         
     
          //Creamos el marcador en el mapa con sus propiedades
          //para nuestro obetivo tenemos que poner el atributo draggable en true
          //position pondremos las mismas coordenas que obtuvimos en la geolocalización
          
       
}

function createMarker(){
      marker = new google.maps.Marker({
            map: map,
            draggable: true,
            position: new google.maps.LatLng(lat,lng),
     
          });
 }
 
 
/*function setMapa (coords)
{   
      //Se crea una nueva instancia del objeto mapa
      var map = new google.maps.Map(document.getElementById('map'),
      {
        zoom: 16,
        center:new google.maps.LatLng(coords.lat,coords.lng),
 
      }); 
 
      //Creamos el marcador en el mapa con sus propiedades
      //para nuestro obetivo tenemos que poner el atributo draggable en true
      //position pondremos las mismas coordenas que obtuvimos en la geolocalización
      marker = new google.maps.Marker({
        map: map,
        draggable: true,
        position: new google.maps.LatLng(coords.lat,coords.lng),
 
      });

}*/
 

 
    </script>

<script  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA1oacN_QnqMV83diFiFevMgYGocSq1WcI"></script>
@endsection