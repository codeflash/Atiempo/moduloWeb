@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div style="height:500px; width:500px" id="map">
            
            </div>
            <div>
            		<input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
                    <input type="text" id="lat">
                    <input type="text" id="long">
            </div>
        </div>
    </div>
</div>
<script>

		var marker;          //variable del marcador
		var coords = {};    //coordenadas obtenidas con la geolocalización
		 
		//Funcion principal
		initMap = function () 
		{
		 
		    //usamos la API para geolocalizar el usuario
		        navigator.geolocation.getCurrentPosition(
		          function (position){
		            coords =  {
		              lng: position.coords.longitude,
		              lat: position.coords.latitude
		            };
		            setMapa(coords);  //pasamos las coordenadas al metodo para crear el mapa
		            
		           
		          },function(error){console.log(error);});
		    
		}
		 
		function setMapa (coords)
		{   
		      //Se crea una nueva instancia del objeto mapa
		      var map = new google.maps.Map(document.getElementById('map'),
		      {
		        zoom: 16,
		        center:new google.maps.LatLng(coords.lat,coords.lng),
		 
		      });
		 
		      //Creamos el marcador en el mapa con sus propiedades
		      //para nuestro obetivo tenemos que poner el atributo draggable en true
		      //position pondremos las mismas coordenas que obtuvimos en la geolocalización
		      marker = new google.maps.Marker({
		        map: map,
		        draggable: true,
		        position: new google.maps.LatLng(coords.lat,coords.lng),
		 
		      });

		      map.addListener('click', function(e) {
			    placeMarkerAndPanTo(e.latLng, map);
			  });
			

			

		}
 		
 		function placeMarkerAndPanTo(latLng, map) {
			var marker = new google.maps.Marker({
			    position: latLng,
			    map: map
			});
			var makerposition = marker.getPosition();
			var token = $("#token").val();
			$.ajax({
				url : "http://localhost:8000/map",
				headers : {'X-CSRF-TOKEN' : token},
				type : "POST",
				dataType : "json",
				data: { lat : makerposition.lat() , long : makerposition.lng()}
			});
			
		 
		}

 
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?callback=initMap&&key=AIzaSyA1oacN_QnqMV83diFiFevMgYGocSq1WcI"></script>
@endsection
