<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
use CoRnford\Googlmapper\Facades\MapperFacade;
use Illuminate\Support\Facades\Route;


Route::get('/', function () {
	return view('welcome');
});



Auth::routes();
Route::get('/map', 'MapController@index');
Route::put('/map', 'MapController@store');
Route::post('/map', 'MapController@edit');
Route::get('/map2', 'MapController@update');

Route::get('/home', 'HomeController@index');
