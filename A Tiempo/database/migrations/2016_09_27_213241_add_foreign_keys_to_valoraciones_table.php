<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToValoracionesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('valoraciones', function(Blueprint $table)
		{
			$table->foreign('idUsuario', 'fk_valoraciones_usuarios1')->references('id')->on('users')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('empresas_id', 'fk_valoraciones_empresas1')->references('id')->on('empresas')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('valoraciones', function(Blueprint $table)
		{
			$table->dropForeign('fk_valoraciones_usuarios1');
			$table->dropForeign('fk_valoraciones_empresas1');
		});
	}

}
