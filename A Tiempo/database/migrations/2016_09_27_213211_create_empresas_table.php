<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmpresasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('empresas', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('correo', 200)->nullable()->unique('correo_UNIQUE');
			$table->text('contrasenia', 65535)->nullable();
			$table->string('descripcion', 200)->nullable();
			$table->string('nombre', 200)->nullable();
			$table->string('telefono', 45)->nullable();
			$table->string('direccion', 200)->nullable();
			$table->string('RUC', 100)->nullable();
			$table->integer('idCiudad')->index('fk_empresas_ciudad1_idx');
			$table->integer('idPais')->index('fk_empresas_pais1_idx');
			$table->integer('idEstado')->index('fk_empresas_estado1_idx');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('empresas');
	}

}
