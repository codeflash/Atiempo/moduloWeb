<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToDetallerutaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('detalleruta', function(Blueprint $table)
		{
			$table->foreign('idEmpresa', 'fk_Empresas_has_Rutas_Empresas')->references('id')->on('empresas')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('idRuta', 'fk_Empresas_has_Rutas_Rutas1')->references('id')->on('rutas')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('detalleruta', function(Blueprint $table)
		{
			$table->dropForeign('fk_Empresas_has_Rutas_Empresas');
			$table->dropForeign('fk_Empresas_has_Rutas_Rutas1');
		});
	}

}
