<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVehiculosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('vehiculos', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('descripcion', 200)->nullable();
			$table->string('placa', 45)->unique('placa_UNIQUE');
			$table->integer('idEmpresa')->index('fk_vehiculos_empresas1_idx');
			$table->float('longitud', 10, 0)->nullable();
			$table->float('latitud', 10, 0)->nullable();
			$table->float('velocidad', 10, 0)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('vehiculos');
	}

}
