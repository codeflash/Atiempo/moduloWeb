<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToFotosempresaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('fotosempresa', function(Blueprint $table)
		{
			$table->foreign('idEmpresas', 'fk_fotosEmpresa_empresas1')->references('id')->on('empresas')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('fotosempresa', function(Blueprint $table)
		{
			$table->dropForeign('fk_fotosEmpresa_empresas1');
		});
	}

}
