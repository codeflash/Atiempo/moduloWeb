<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEmpresasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('empresas', function(Blueprint $table)
		{
			$table->foreign('idCiudad', 'fk_empresas_ciudad1')->references('id')->on('ciudades')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('idPais', 'fk_empresas_pais1')->references('id')->on('paises')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('idEstado', 'fk_empresas_estado1')->references('id')->on('estados')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('empresas', function(Blueprint $table)
		{
			$table->dropForeign('fk_empresas_ciudad1');
			$table->dropForeign('fk_empresas_pais1');
			$table->dropForeign('fk_empresas_estado1');
		});
	}

}
